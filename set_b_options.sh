#!/bin/bash
opfile='savedpages.html'

function bNum() {
export bnb=$((bnb + 10))
bArg="&start=$bnb"
}

function bulkPages() {
> savedpages.html
p_range=0
export bcnt=0e
export bnb=10;
choosRange
while [ $bcnt -lt $p_range ];do
    bNum
    echo "---Getting Page $bnb"
    echo " "
    echo "$url$bArg"
    queryPages
    echo " "
    bcnt=$[bcnt + 10]
done
echo "Saved to file"

}

function choosRange() {
    read -r -p 'Save file: ' opfile
    read -r -p 'Range start: ' bcnt
    export bcnt
    echo " "
    read -r -p 'Range finish: ' p_range 
    export p_range
    echo "from: $bcnt to: $p_range"
}

function queryPages() {
phantomjs printSource.js --ssl-protocol=any "$url$bArg" | grep -Eo '/url.q=(http|https)://[^*"]+' | grep -Eo '(http|https)://[^*"]+' | awk '!seen[$0]++' | awk -F'&amp' '{print $1}' >> $opfile
sed -i "/http:\/\/webcache.googleusercontent.com/d" $opfile
xterm -e tail -f $opfile &
}

function pageNumber() {
> pagenumb.txt
url="https://google.com/search?q=$search$pArg"

phantomjs printSource.js "$url" >> pagenumb.txt
}

PS3='Option: '
options=("Bulk Pages " "Back")
select opt in "${options[@]}"
do
    case $opt in
        "Bulk Pages ")
            echo "Running bulk pages"
            bulkPages
            ;;
        "Back")
            ./getpages.sh
            ;;
        *) echo invalid option;;
    esac
done