#!/bin/bash
read -p 'Search: ' search
echo "-------------------------"
echo " "
export search="$search"
export url="https://google.com/search?q=$search"

function getPage() {
> output.html

phantomjs printSource.js "$url" | grep -Eo '/url.q=(http|https)://[^*"]+' | awk -F'&amp' '{print $1}' >> output.html
sed -i "/http:\/\/webcache.googleusercontent.com/d" output.html
}

getPage

function getLinks() {

links=$(grep -Eo '(http|https)://[^*"]+' output.html | awk '!seen[$0]++')
echo "$links"
echo " "
}

getLinks


./getpages.sh

##something to get to another page https://www.google.com/search?q=magic+cards&start=20
