#!/bin/bash
function setNum() {
pArg="&start=$pnumb"
}

function getPage() {
> output.html
phantomjs printSource.js "$url" | grep -Eo '/url.q=(http|https)://[^*"]+' | awk -F'&amp' '{print $1}' >> output.html
sed -i "/http:\/\/webcache.googleusercontent.com/d" output.html
}

function getLinks() {
links=$(grep -Eo '(http|https)://[^*"]+' output.html | awk '!seen[$0]++')
echo "$links"
echo " "
}

function runQuery() {
setNum
url="https://google.com/search?q=$search$pArg"
getPage
getLinks
getPages
}

function opxions() {
./set_b_options.sh
}

function getPages() {
pnumb=""

read -r -p "---Page: " pnumb

if [ "$pnumb" = '02' ]; then
    echo " "
    pnumb='30'   
    runQuery
   
elif [ "$pnumb" = '03' ]; then
	echo " "
	pnumb='40'
	runQuery

elif [ "$pnumb" = '04' ]; then
	echo " "
	pnumb='50'
	runQuery

elif [ "$pnumb" = '05' ]; then
	echo " "
	pnumb='60'
	runQuery

elif [ "$pnumb" = '06' ]; then
	echo " "
	pnumb='70'
	runQuery

elif [ "$pnumb" = '07' ]; then
	echo " "
	pnumb='60'
	runQuery

elif [ "$pnumb" = '08' ]; then
	echo " "
	pnumb='70'
	runQuery

elif [ "$pnumb" = '09' ]; then
	echo " "
	pnumb='80'
	runQuery

elif [ "$pnumb" = '10' ]; then
	echo " "
	pnumb='90'
	runQuery

elif [ "$pnumb" = '11' ]; then
	echo " "
	pnumb='100'
	runQuery

elif [ "$pnumb" = '12' ]; then
	echo " "
	pnumb='110'
	runQuery

elif [ "$pnumb" = '13' ]; then
	echo " "
	pnumb='120'
	runQuery

elif [ "$pnumb" = '14' ]; then
	echo " "
	pnumb='130'
	runQuery

elif [ "$pnumb" = '15' ]; then
	echo " "
	pnumb='140'
	runQuery

elif [ "$pnumb" = '16' ]; then
	echo " "
	pnumb='150'
	runQuery

elif [ "$pnumb" = '17' ]; then
	echo " "
	pnumb='160'
	runQuery

elif [ "$pnumb" = 'P' ]; then
	echo " "
	opxions

elif [ "$pnumb" = 'N' ]; then
	echo " "
	clear
	./index.sh

elif [ "$pnumb" = 'H' ]; then
	echo " "
	echo "Help"
	echo "Type P for bulk options"
	echo "Type N for new search"

else
	echo "Type a number, ex: 01"
	getPages
fi
}

getPages